Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer

Conversion from bash to batch

This files contains mapping of bash command for batch and sometimes to OSX command

# Expression bash  => batch => osx
## Declaration
- `var="foo"` => `set "var=foo"`
- `var=1` => `set /a "var=1"`
- `var=true` => `set "var=1==1"`
- `var=false` => `set "var=1==0"`

## Condition:
- `if [ -z ${var} ] then #do smtg fi` => `if not defined var #do smtg`

## File manipulation
- `mkdir -p /path/to/file` => `mkdir path\to\file`
- `mkdir -p ${path}` => `mkdir %path%` 

## Process
- `exit 101` => `exit /b 101`
- `if [ $? = 0] then <cmd>` => `if not errorLevel 1 (<cmd>)` 
- `var=$1` => `set "var=%1"`
- `>/dev/null` => ` > NUL`
- `2>&1` => `2>&1`  

## Other
- `sha256sum -c filename` => `certUtil -hashfile filename SHA256` => `shasum -a 256 filename` 
- `wget -O path/to/localFile <URL>` => `curl -o path\to\localFile <URL>` 
- `ping -c` => `ping -n`
- `# comment` => ` rem comment` but not efficient at all. Thus use `:: comment`. 
- `sleep 1` => `timeout 1 >nul`
- `grep` => `findstr`
- use `~` in var to handle space in path. Ex: `%~1`

## Loop
- `while <condition>` => `:myFunction if <condition> (goto :myFunction)` (it's a recursive  function) 