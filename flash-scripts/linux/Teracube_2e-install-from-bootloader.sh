#!/bin/bash

# Copyright (C) 2021 ECORP SAS - Author: Vincent Bourgmayer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Parameter
# $1: DEVICE_ID device id
# $2: ARCHIVE_PATH path to archive
# $3: fastboot folder path
# $4: Java folder path


# Exit status
# - 0 : device flashed
# - 1 : generic error
# - 10: can't unpack system.img
# - 11: can't wipe data
# - 12: can't flash boot
# - 13: can't flash recovery
# - 14: can't flash vbmeta
# - 15: can't flash vbmeta_system
# - 16: can't flash vbmeta_vendor
# - 17: can't reboot on fastboot
# - 18: can't flash system
# - 19: can't flash product
# - 20: can't flash vendor
# - 101 : DEVICE_ID missing
# - 102 : ARCHIVE_PATH missing
# - 103 : fastboot folder path missing

DEVICE_ID=$1
ARCHIVE_PATH=$2
FASTBOOT_FOLDER_PATH=$3
JAVA_FOLDER_PATH=$4

# Check serial number has been provided
if [ -z "$DEVICE_ID" ]
then
  exit 101
fi

# check path to rom has been provided
if [ -z "$ARCHIVE_PATH" ]
then
  exit 102
fi

#check path to adb/fasboot has been provided
if [ -z "$FASTBOOT_FOLDER_PATH" ]
then
  exit 103
fi

# Check java folder has been provided
if [ -z "$JAVA_FOLDER_PATH" ]
then
  exit 104
fi


# Build fastboot path
FASTBOOT_PATH=${FASTBOOT_FOLDER_PATH}"fastboot"

# Build java jar path
JAR_PATH=${JAVA_FOLDER_PATH}"/bin/jar"

# Build archive folder path
ARCHIVE_FOLDER_PATH=$(dirname "$ARCHIVE_PATH")"/"

# unzip for system.img
cd "$ARCHIVE_FOLDER_PATH" || exit 104

if ! "$JAR_PATH" -x -v -f "$ARCHIVE_PATH" ;
then 
  exit 10
fi

echo "unpacked archive"

sleep 1

# Wipe user data
if ! "$FASTBOOT_PATH" -w ;
then 
  exit 11
fi

echo "user data wiped"
sleep 5

# Flash the device
if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash boot boot.img
then
  exit 12
fi
sleep 1
echo "Flashed boot"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash recovery recovery.img
then
  exit 13
fi
sleep 1
echo "Flashed recovery"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash vbmeta vbmeta.img
then
  exit 14
fi
sleep 1
echo "Flashed vbmeta"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash vbmeta_system vbmeta_system.img
then
  exit 15
fi
sleep 1
echo "Flashed vbmeta_system"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash vbmeta_vendor vbmeta_vendor.img
then
  exit 16
fi
sleep 1
echo "Flashed vbmeta_vendor"


if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" reboot fastboot
then
  exit 17
fi
sleep 6
echo "Rebooted on fastboot"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash system system.img
then
  exit 18
fi
sleep 1
echo "Flashed system"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash product product.img
then
  exit 19
fi
sleep 1
echo "Flashed product"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash vendor vendor.img
then
  exit 20
fi
sleep 1
echo "Flashed vendor"