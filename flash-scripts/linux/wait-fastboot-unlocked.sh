#!/bin/bash

# Copyright (C) 2020 - Author: Ingo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# TODO: What if 2 devices detected?
# Parameter
# $1: The folder where fastboot runnable is stored

# Exit status
# - 0 : Device in fastboot mode detected and bootloader unlocked
# - 1 : Error

FASTBOOT_FOLDER_PATH=$1
FASTBOOT_PATH=${FASTBOOT_FOLDER_PATH}"fastboot"

echo "fastboot path: $FASTBOOT_PATH"

while [ "$($FASTBOOT_PATH getvar unlocked 2>&1 | grep -q yes; echo $?)" = 1 ]
do
  sleep 2s
done

sleep 5s
echo "fastboot mode detected, phone is unlocked"