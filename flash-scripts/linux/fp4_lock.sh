#!/bin/bash

# Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer
# Updated ff2u
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Parameter

# $1: ADB_FOLDER_PATH: the path where runnable adb is stored

# Exit status
# - 0 : success
# - 1 : Error
# - 102 : locking the bootloader failed

FASTBOOT_FOLDER_PATH=$1
FASTBOOT_PATH=${FASTBOOT_FOLDER_PATH}"fastboot"

# Lock the bootloader
if ! "$FASTBOOT_PATH" flashing lock  ;
then exit 102 ; fi

#Then we wait that it left this state
while [ "$($FASTBOOT_PATH devices | grep -q fastboot; echo $?)" = 0 ]
do
    sleep 1s
done
