#!/bin/bash

# Copyright (C) 2020 - Author: Ingo; update and adoption to FP4, 2022 by ff2u
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Parameter
# $1: ARCHIVE_PATH path to the /e/ archive to flash
# $2: FASTBOOT_FOLDER_PATH: the path where runnable fastboot is stored
# $3: JAVA_FOLDER_PATH: the path where runnable jar is stored (to unpack ZIP file)

# Exit status
# - 0 : /e/ installed
# - 1 : user data wipe failed
# - 2 : flashing of a partition failed
# - 4 : setting slot failed
# - 5 : flashing of a secure partition failed
# - 101 : ARCHIVE_PATH missing
# - 102 : archive could not be unpacked

ARCHIVE_PATH=$1
ARCHIVE_FOLDER_PATH=$(dirname "$1")"/"
FASTBOOT_FOLDER_PATH=$2
FASTBOOT_PATH=${FASTBOOT_FOLDER_PATH}"fastboot"
JAVA_FOLDER_PATH=$3
JAR_PATH=${JAVA_FOLDER_PATH}"/bin/jar"

echo "archive path : $ARCHIVE_PATH"
echo "archive folder path : $ARCHIVE_FOLDER_PATH"
echo "fastboot path : $FASTBOOT_PATH"
echo "jar path : $JAR_PATH"

if [ -z "$ARCHIVE_PATH" ]
then
  exit 101
fi

cd "$ARCHIVE_FOLDER_PATH"

sleep 1

if ! "$JAR_PATH" -x -v -f "$ARCHIVE_PATH" ;
then exit 102 ; fi

echo "unpacked archive"

sleep 1

echo "=== Flash slot a"

echo "== start with one critical slot to check if critical is unlocked"

if ! "$FASTBOOT_PATH" flash xbl_config_a xbl_config.img ; then exit 5 ; fi

echo "flashed config"

sleep 1

echo "== flash uncritical slots"

if ! "$FASTBOOT_PATH" flash bluetooth_a bluetooth.img ; then exit 2 ; fi

echo "flashed bluetooth"

sleep 1

if ! "$FASTBOOT_PATH" flash dsp_a dsp.img ; then exit 2 ; fi

echo "flashed dsp"

sleep 1

if ! "$FASTBOOT_PATH" flash modem_a modem.img ; then exit 2 ; fi

echo "flashed modem"

sleep 1

if ! "$FASTBOOT_PATH" flash boot_a boot.img ; then exit 2 ; fi

echo "flashed boot"

sleep 1

if ! "$FASTBOOT_PATH" flash recovery_a recovery.img ; then exit 2 ; fi

echo "flashed recovery"

sleep 1

if ! "$FASTBOOT_PATH" flash dtbo_a dtbo.img ; then exit 2 ; fi

echo "flashed dtbo"

sleep 1

if ! "$FASTBOOT_PATH" flash vbmeta_system_a vbmeta_system.img ; then exit 2 ; fi

echo "flashed vbmeta_system"

sleep 1

if ! "$FASTBOOT_PATH" flash vbmeta_a vbmeta.img ; then exit 2 ; fi

echo "flashed vbmeta"

sleep 1

if ! "$FASTBOOT_PATH" flash featenabler_a featenabler.img ; then exit 2 ; fi

echo "flashed featenabler"

sleep 1

if ! "$FASTBOOT_PATH" flash core_nhlos_a core_nhlos.img ; then exit 2 ; fi

echo "flashed core_nhlos"

sleep 1

echo "=== Flash critical slots"

if ! "$FASTBOOT_PATH" flash devcfg_a devcfg.img ; then exit 5 ; fi

echo "flashed devcfg"

sleep 1

if ! "$FASTBOOT_PATH" flash xbl_a xbl.img ; then exit 5 ; fi

echo "flashed xbl"

sleep 1

if ! "$FASTBOOT_PATH" flash tz_a tz.img ; then exit 5 ; fi

echo "flashed tz"

sleep 1

if ! "$FASTBOOT_PATH" flash hyp_a hyp.img ; then exit 5 ; fi

echo "flashed hyp"

sleep 1

if ! "$FASTBOOT_PATH" flash keymaster_a keymaster.img ; then exit 5 ; fi

echo "flashed keymaster"

sleep 1

if ! "$FASTBOOT_PATH" flash abl_a abl.img ; then exit 5 ; fi

echo "flashed abl"

sleep 1

if ! "$FASTBOOT_PATH" flash aop_a aop.img ; then exit 5 ; fi

echo "flashed aop"

sleep 1

if ! "$FASTBOOT_PATH" flash imagefv_a imagefv.img ; then exit 5 ; fi

echo "flashed imagefv"

sleep 1

if ! "$FASTBOOT_PATH" flash multiimgoem_a multiimgoem.img ; then exit 5 ; fi

echo "flashed multiimgoem"

sleep 1

if ! "$FASTBOOT_PATH" flash qupfw_a qupfw.img ; then exit 5 ; fi

echo "flashed qupfw"

sleep 1

if ! "$FASTBOOT_PATH" flash uefisecapp_a uefisecapp.img ; then exit 5 ; fi

echo "flashed uefisecapp"

sleep 1

echo "=== Flash slot b"

echo "== start with one critical slot to check if critical is unlocked"

if ! "$FASTBOOT_PATH" flash xbl_config_b xbl_config.img ; then exit 5 ; fi

echo "flashed config"

sleep 1

echo "== flash uncritical slots"

if ! "$FASTBOOT_PATH" flash bluetooth_b bluetooth.img ; then exit 2 ; fi

echo "flashed bluetooth"

sleep 1

if ! "$FASTBOOT_PATH" flash dsp_b dsp.img ; then exit 2 ; fi

echo "flashed dsp"

sleep 1

if ! "$FASTBOOT_PATH" flash modem_b modem.img ; then exit 2 ; fi

echo "flashed modem"

sleep 1

if ! "$FASTBOOT_PATH" flash boot_b boot.img ; then exit 2 ; fi

echo "flashed boot"

sleep 1

if ! "$FASTBOOT_PATH" flash recovery_b recovery.img ; then exit 2 ; fi

echo "flashed recovery"

sleep 1

if ! "$FASTBOOT_PATH" flash dtbo_b dtbo.img ; then exit 2 ; fi

echo "flashed dtbo"

sleep 1

if ! "$FASTBOOT_PATH" flash vbmeta_system_b vbmeta_system.img ; then exit 2 ; fi

echo "flashed vbmeta_system"

sleep 1

if ! "$FASTBOOT_PATH" flash vbmeta_b vbmeta.img ; then exit 2 ; fi

echo "flashed vbmeta"

sleep 1

if ! "$FASTBOOT_PATH" flash featenabler_b featenabler.img ; then exit 2 ; fi

echo "flashed featenabler"

sleep 1

if ! "$FASTBOOT_PATH" flash core_nhlos_b core_nhlos.img ; then exit 2 ; fi

echo "flashed core_nhlos"

sleep 1

echo "=== Flash critical slots"

if ! "$FASTBOOT_PATH" flash devcfg_b devcfg.img ; then exit 5 ; fi

echo "flashed devcfg"

sleep 1

if ! "$FASTBOOT_PATH" flash xbl_b xbl.img ; then exit 5 ; fi

echo "flashed xbl"

sleep 1

if ! "$FASTBOOT_PATH" flash tz_b tz.img ; then exit 5 ; fi

echo "flashed tz"

sleep 1

if ! "$FASTBOOT_PATH" flash hyp_b hyp.img ; then exit 5 ; fi

echo "flashed hyp"

sleep 1

if ! "$FASTBOOT_PATH" flash keymaster_b keymaster.img ; then exit 5 ; fi

echo "flashed keymaster"

sleep 1

if ! "$FASTBOOT_PATH" flash abl_b abl.img ; then exit 5 ; fi

echo "flashed abl"

sleep 1

if ! "$FASTBOOT_PATH" flash aop_b aop.img ; then exit 5 ; fi

echo "flashed aop"

sleep 1

if ! "$FASTBOOT_PATH" flash imagefv_b imagefv.img ; then exit 5 ; fi

echo "flashed imagefv"

sleep 1

if ! "$FASTBOOT_PATH" flash multiimgoem_b multiimgoem.img ; then exit 5 ; fi

echo "flashed multiimgoem"

sleep 1

if ! "$FASTBOOT_PATH" flash qupfw_b qupfw.img ; then exit 5 ; fi

echo "flashed qupfw"

sleep 1

if ! "$FASTBOOT_PATH" flash uefisecapp_b uefisecapp.img ; then exit 5 ; fi

echo "flashed uefisecapp"

sleep 1

echo "=== Flash common slot"

if ! "$FASTBOOT_PATH" flash super super.img ; then exit 2 ; fi

echo "flashed super"

sleep 1

echo "=== Wipe user data and meta data in 5 seconds..."

sleep 3

if ! "$FASTBOOT_PATH" erase userdata ; then exit 1 ; fi

sleep 1

if ! "$FASTBOOT_PATH" erase metadata ; then exit 1 ; fi

echo "user and meta data wiped"

sleep 3

echo "Select slot a for booting..."

if ! "$FASTBOOT_PATH" --set-active=a ; then exit 4 ; fi

echo "boot from slot a selected"

