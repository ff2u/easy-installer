:: Copyright (C) 2021 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: DEVICE_ID device id
:: $2: ARCHIVE_PATH path to archive
:: $3: fastboot folder path
:: $4: Java folder path


:: Exit status
:: - 0 : device flashed
:: - 1 : generic error
:: - 10: can't unpack system.img
:: - 11: can't wipe data
:: - 12: can't flash boot
:: - 13: can't flash recovery
:: - 14: can't flash vbmeta
:: - 15: can't flash vbmeta_systel
:: - 16: can't flash vbmeta_vendor
:: - 17: can't reboot to fastboot
:: - 18: can't flash system
:: - 19: can't flash product
:: - 20: can't flash vendor
:: - 101 : DEVICE_ID missing
:: - 102 : ARCHIVE_PATH missing
:: - 103 : fastboot folder path missing

set DEVICE_ID="%1"
set ARCHIVE_PATH=%~2
set FASTBOOT_FOLDER_PATH=%~3
set JAVA_FOLDER_PATH=%~4

if not defined %DEVICE_ID (
  exit /b 101
)

if not defined %ARCHIVE_PATH (
  exit /b 102
)

if not defined %FASTBOOT_FOLDER_PATH (
  exit /b 103
)

:: Check java folder has been provided
if not defined %JAVA_FOLDER_PATH (
  exit /b 104
)

set FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"


set JAR_PATH="%JAVA_FOLDER_PATH%/bin/jar"

:: Build archive folder path
for %%a in ("%ARCHIVE_PATH%") do (
	set ARCHIVE_FOLDER_PATH=%%~dpa"
	echo %ARCHIVE_FOLDER_PATH%
)
:: unzip for system.img
cd "%ARCHIVE_FOLDER_PATH%"

%JAR_PATH% -x -v -f "%ARCHIVE_PATH%"
if errorLevel 1 ( exit /b 10 )

echo "unpacked archive"

ping 127.0.0.1 -n 1 -w 10000 >NUL

%FASTBOOT_PATH% -w
if errorLevel 1 ( exit /b 11 )

echo "user data wiped"

ping 127.0.0.1 -n 5 -w 10000 >NUL

%FASTBOOT_PATH% -s %DEVICE_ID% flash boot boot.img
if errorLevel 1 ( exit /b 12 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed boot"

%FASTBOOT_PATH% -s %DEVICE_ID% flash recovery recovery.img
if errorLevel 1 ( exit /b 13 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed recovery"

%FASTBOOT_PATH% -s %DEVICE_ID% flash vbmeta vbmeta.img
if errorLevel 1 ( exit /b 14 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed vbmeta"

%FASTBOOT_PATH% -s %DEVICE_ID% flash vbmeta_system vbmeta_system.img
if errorLevel 1 ( exit /b 15 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed vbmeta_system"

%FASTBOOT_PATH% -s %DEVICE_ID% flash vbmeta_vendor vbmeta_vendor.img
if errorLevel 1 ( exit /b 16 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed vbmeta_vendor"

%FASTBOOT_PATH% -s %DEVICE_ID% reboot fastboot
ping 127.0.0.1 -n 6 -w 10000 >NUL
echo "Rebooted on fastboot"

%FASTBOOT_PATH% -s %DEVICE_ID% flash system system.img
if errorLevel 1 ( exit /b 18 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed vbmeta_vendor"

%FASTBOOT_PATH% -s %DEVICE_ID% flash product product.img
if errorLevel 1 ( exit /b 19 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed product"

%FASTBOOT_PATH% -s %DEVICE_ID% flash vendor vendor.img
if errorLevel 1 ( exit /b 20 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed vendor"
exit /b 0