:: Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: DEVICE_ID device identifier
:: $2: ARCHIVE_PATH path to the /e/ archive to flash
:: $3: ADB_FOLDER_PATH: the path where runnable adb is stored
:: Exit status
:: - 0 : /e/ installed
:: - 1 : Problems occurred (adb returns only 1 if an error occurs)
:: - 2 : Problems occurred during file transfer
:: - 3 : Problems occurred /e/ installation
:: - 101 : DEVICE_ID missing
:: - 102 : ARCHIVE_PATH missing

set DEVICE_ID="%1"
set ARCHIVE_PATH="%~2"

set ADB_FOLDER_PATH=%~3
set ADB_PATH="%ADB_FOLDER_PATH%adb"
:: get basename of archive
for %%a in ("%ARCHIVE_PATH%") do (
	set "ARCHIVE_NAME=%%~nxa"
	echo %ARCHIVE_NAME%
)

if not defined %DEVICE_ID (
  exit /b 101
)

if not defined %ARCHIVE_PATH (
  exit /b 102
)

%ADB_PATH% -s %DEVICE_ID% shell twrp wipe system
%ADB_PATH% -s %DEVICE_ID% shell twrp wipe cache
%ADB_PATH% -s %DEVICE_ID% shell twrp wipe data
%ADB_PATH% -s %DEVICE_ID% push  %ARCHIVE_PATH% sdcard

if errorLevel 1 ( exit /b 2 )

%ADB_PATH% -s %DEVICE_ID% shell twrp install /sdcard/%ARCHIVE_NAME%

if errorLevel 1 ( exit /b 3 )

::Timeout 1
ping 127.0.0.1 -n 2 -w 1000 >NUL

%ADB_PATH% -s %DEVICE_ID% shell rm /sdcard/%ARCHIVE_NAME%

ping 127.0.0.1 -n 2 -w 1000 >NUL

exit /b 0