:: Copyright (C) 2021 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: serial number of the device
:: $2: The folder where fastboot runnable is stored

:: Exit status
:: - 0 : Device in fastboot mode detected
:: - 1 : unknown Error
:: - 10: rebooting in bootloader failed
:: - 101: Device ID not provided
:: - 102: Missing path to adb and fastboot

set DEVICE_ID="%1"
set ADBFASTBOOT_FOLDER_PATH=%~2

if not defined %DEVICE_ID (
  exit /b 101
)

if not defined %ADBFASTBOOT_FOLDER_PATH (
  exit /b 102
)

set FASTBOOT_PATH="%ADBFASTBOOT_FOLDER_PATH%fastboot"
set ADB_PATH="%ADBFASTBOOT_FOLDER_PATH%adb"


%ADB_PATH% -s %DEVICE_ID% reboot bootloader
if errorlevel 1 ( exit /b 10 )


:fastboot_detect
%FASTBOOT_PATH% devices 2>&1 | findstr fastboot
if errorLevel 1 (
	timeout 3 >nul 2>&1
	goto :fastboot_detect
) 

call fastboot_detect

timeout 5 >nul 2>&1
echo "fastboot mode detected"

exit /b 0