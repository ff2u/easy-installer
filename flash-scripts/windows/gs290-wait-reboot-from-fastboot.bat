:: Copyright (C) 2021 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: ADB_FOLDER_PATH: the path where runnable adb is stored

:: Exit status
:: - 0 : success
:: - 1 : unknown error
:: - 10 : fastboot reboot command failed
:: - 101 : no device found in fastboot

set FASTBOOT_FOLDER_PATH=%~1
set FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"

echo "fastboot path: %FASTBOOT_FOLDER_PATH%"


%FASTBOOT_PATH% devices 2>&1 | findstr fastboot
if %errorLevel% NEQ 0 (
	echo "Device not detected in fastboot"
	exit /b 101
) 

%FASTBOOT_PATH% reboot
if %errorlevel% NEQ 0 ( exit /b 10 )

:fastboot_detect
%FASTBOOT_PATH% devices 2>&1 | findstr fastboot
if %errorLevel%  EQU 0 (
	ping 127.0.0.1 -n 2 -w 1000 >NUL
	goto :fastboot_detect
) else (exit /b 0)

call :fastboot_detect

exit /b 0
