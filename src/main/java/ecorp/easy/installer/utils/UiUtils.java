/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecorp.easy.installer.utils;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * this class contains method that apply effect on User interface
 * @author vincent
 */
public abstract class UiUtils {
    private final static Logger logger = LoggerFactory.getLogger(UiUtils.class);
    
    /**
     * Makes Node not managed and not visible
     * It is used to hide an element on the view
     * @param node 
     */
    public final static void hideNode(final Node node){
        node.setManaged(false);
        node.setVisible(false);
    }
    
    /**
     * Makes a node visible and managed
     * It is used to display an element which has been hidden
     * @param node 
     */
    public final static void showNode(final Node node){
        node.setManaged(true);
        node.setVisible(true);
    }
    
    /**
     * Build a Fade in or Fade out animation for a specified Node
     * @param node The node that will fade
     * @param isFadeIn true if it's a fade in animation. False if it's a fade out animation.
     * @return FadeTransition the animation
     */
    public final static FadeTransition buildFadeTransition(final Node node, boolean isFadeIn){
        final FadeTransition transition = new FadeTransition(Duration.millis(300), node);
        transition.setFromValue(isFadeIn ? 1.0 : 0.0);
        transition.setToValue(isFadeIn ? 0.0 : 1.0);

        return transition;
    }
    
    
    /**
     * could have a suffix: on the same node!
     * CREATE & Play fade in animation then fade out animation.
     * @param node The node whichs will fade in/fadeout. it is used for fade in & fade out!
     * @param handler1 Handler applied at the end of fade in transition
     * @param handler2 handler applied at the end of fade out transition
     */
    public final static void playFadeAnimation(final Node node, final EventHandler<ActionEvent> handler1, final EventHandler<ActionEvent>handler2){
        FadeTransition transition = new FadeTransition(Duration.millis(300), node);
        transition.setFromValue(1.0);
        transition.setToValue(0.0);
        transition.setAutoReverse(true);
        transition.play();
        
        transition.setOnFinished(e -> {
            handler1.handle(e);
            
            transition.setRate(-1.0);
            transition.play();
            transition.setOnFinished(handler2); 
        });
    }
    
    /**
     * Create an Image instance and load a file to fill it.
     * 
     * @param fileName
     * @return null if there is error when loading file name 
     */
    public final static Image loadImage(String fileName){
        Image result = null;
        try{
            result = new Image(UiUtils.class.getResourceAsStream("/images/"+fileName));
        }catch (Exception e) {
            logger.warn("loadImage(), image's file name = {}, error = {}", fileName, e.toString());
        }
        return result;
    }
    
}