/*
 * Copyright 2021 - ECORP SAS 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.utils;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.models.Command;
import ecorp.easy.installer.models.steps.BasicStep;
import ecorp.easy.installer.models.steps.CustomExecutableStep;
import ecorp.easy.installer.models.steps.CustomStep;
import ecorp.easy.installer.models.steps.ExecutableStep;
import ecorp.easy.installer.models.steps.IStep;
import ecorp.easy.installer.models.steps.LoadStep;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class in charge of parsing yaml config file
 * into IStep's implementation
 * 
 * I wonder, if I should make it a singleton...
 * This class is perfect candidate for unit test
 * however, since its methods are static it might
 * cause a problem (I'm thinking of mocking)
 * @author vincent Bourgmayer
 */
public class ConfigParser {
    private final static Logger logger = LoggerFactory.getLogger(ConfigParser.class);
    
    /**
     * Parse steps from yaml (both for preparation and flashing)
     * @param yaml The data map load from yaml file
     * @return an HashMap of IStep
     * @throws ParseException 
     */
    public static HashMap<String, IStep> parseSteps(Map yaml)throws ParseException, NumberFormatException{
        logger.debug("parseSteps(yaml)");
        final HashMap<String, IStep> result = new HashMap<>();
        for(String key : (Set<String>) yaml.keySet() ){
            IStep step = ConfigParser.parseStep( (Map) yaml.get(key) );
            result.put(key,step);
        }
        return result;
    }

    
    /**
     * Create a single Step instance from a Map<String, String> containing yaml content
     * @param yaml
     * @return the IStep implementation's instance
     * @throws ParseException
     * @throws NumberFormatException 
     */
    public static IStep parseStep(Map yaml) throws ParseException, NumberFormatException, NullPointerException{
        IStep result = null;
        
        String type = (String) yaml.get("type");
        logger.debug("--step type:"+type);
        switch(type){
            case "load":
                result = parseLoadStep(yaml, type);
                // example: twrp or /e/ installation
                break;
            case "custom":
                result = parseCustomStep(yaml, type);
                // Example: enable DevOption, enable ADB, enable MTP
                // S9: OEM unlock
                // FP3: update phone & oem unlock
                // GS290 update stockrom
                break;
            case "custom-executable":
                result = parseCustomExecutableStep(yaml, type);
                //Example: reboot from download mode into twrp, etc.
                break;
            case "executable":
                result = parseExecutableStep(yaml, type);
                //Example: Device detection
                break;
            default:
                result = parseBasicStep(yaml, type);
                break;
        }
        return result;
    }
    
    /* NOTE for BELOW METHODS:
    * Using yaml.getOrDefault(key, defaultValue)
    * does only protect from missing value in yaml
    * it doesn't protect from a value willingly setted
    * to null
    */
    
    
    
    
    /**
     * create a basic step from yaml.
     * This is perfect to integrate fixed step like deviceDetection
     * @param yaml the yaml input
     * @param type type of the step
     * @return Step object with nextStepKey, stepNumber and type
     */
    private static BasicStep parseBasicStep(Map yaml, String type) throws ParseException, ClassCastException{
        final int stepNumber = (int) yaml.get("stepNumber");
        final String nextStepKey = (String) yaml.get("nextStepKey");
        
        return new BasicStep(type, nextStepKey, stepNumber);
    }


    /**
     * Parse yaml content into LoadStep
     * example of this kind of step is /e/ or TWRP installation
     * @param yaml yaml content
     * @param type Type of step
     * @return LoadStep the step
     */
    private static LoadStep parseLoadStep(Map yaml, String type) throws ParseException, ClassCastException{
        final int stepNumber = (int)yaml.get("stepNumber");
        final String nextStepKey = (String) yaml.get("nextStepKey");
        final String titleKey = (String) yaml.get("titleKey");
        final String titleIconName = (String) yaml.getOrDefault("titleIconName", "");
        final ArrayList<String> instructions = (ArrayList<String>) yaml.getOrDefault("instructions", new ArrayList<String>());        
        final int averageTime = (int) yaml.get("averageTime");

        final Command cmd = parseCommand(yaml);
        LoadStep result = new LoadStep(type, nextStepKey, stepNumber , 
                titleKey, titleIconName, instructions, cmd, averageTime);
        return result;
    }   

    /**
     * Parse yaml content into CustomStep object
     * Example of this kind of test is when you tell
     * the user to enable developer mode.
     * @param yaml Map yaml content
     * @param type Type of step
     * @return CustomStep object
     */
    private static CustomStep parseCustomStep(Map yaml, String type) throws ParseException, ClassCastException{
        logger.debug("parseCustomStep(yaml)");
        int stepNumber = (int)yaml.get("stepNumber");
        String nextStepKey = (String) yaml.get("nextStepKey");
        String titleKey = (String) yaml.get("titleKey");
        String titleIconName = (String) yaml.get("titleIconName");
        ArrayList<String> instructions = (ArrayList<String>) yaml.getOrDefault("instructions", new ArrayList<String>());       
        
        CustomStep result = new CustomStep(type, nextStepKey, stepNumber, titleKey, titleIconName, instructions);
        return result;
    }
    
    /**
     * Parse yaml content into ExecutableStep
     * example of this kind of step is the step 
     * to detect device
     * @param yaml yaml content
     * @param type Type of step
     * @return ExecutableStep the step
     */
    private static ExecutableStep parseExecutableStep(Map yaml, String type) throws ParseException, ClassCastException{
        final int stepNumber = (int)yaml.get("stepNumber");
        final String nextStepKey = (String) yaml.get("nextStepKey");
        final Command cmd = parseCommand(yaml);
        ExecutableStep result = new ExecutableStep(type, nextStepKey, stepNumber, cmd);
        return result;
    }

    /**
     * Parse yaml content into Command object
     * @TODO rewrite when splitting command between data and job
     * @param yaml Map yaml content 
     * @return Command object
     */
    private static Command parseCommand(Map yaml) throws ParseException, ClassCastException{
        final String commandBase = AppConstants.getScriptsFolderPath()+(String) yaml.get("script")+( AppConstants.isWindowsOs() ? ".bat" : ".sh" ) ;

        final String outputKey = (String) yaml.getOrDefault("outputKey", null);
        final LinkedHashMap<String, String> parameters = (LinkedHashMap) yaml.get("parameters");
        final HashMap<Integer, String> okCodes = (HashMap) yaml.getOrDefault("okCodes", null);
        final HashMap<Integer, String> koCodes = (HashMap) yaml.getOrDefault("koCodes", null);
        
        final Command result = new Command(commandBase);
        result.setOutputKey(outputKey);
        result.setKoCodes(koCodes);
        result.setOkCodes(okCodes);
        result.setParameters(parameters);
  
        return result;
    }
    
    /**
     * Parse yaml content into ExecutableCustomStep
     * example of this kind of step is the step on
     * Samsung galaxy device when user must leave 
     * download mode to start on TWRP
     * @param yaml yaml content
     * @param type Type of step
     * @return ExecutableCustomStep the step
     */
    private static CustomExecutableStep parseCustomExecutableStep(Map yaml, String type) throws ParseException, ClassCastException{
        final int stepNumber = (int)yaml.get("stepNumber");
        final String nextStepKey = (String) yaml.get("nextStepKey");
        final String titleKey = (String) yaml.get("titleKey");
        final String titleIconName = (String) yaml.getOrDefault("titleIconName", "");
        final ArrayList<String> instructions = (ArrayList<String>) yaml.getOrDefault("instructions", new ArrayList<String>());     
        final Command cmd = parseCommand(yaml);
        CustomExecutableStep result = new CustomExecutableStep(type, nextStepKey, stepNumber,
                titleKey, titleIconName, instructions, cmd);
        return result;
    }
    
    
    /**
     * Parse Sources To Download from Yaml file
     * @param yaml data load from Yaml
     * @return HashMap containing sources's URL & local path to store
     * @throws java.text.ParseException
     */
    public static HashMap<String, String> parseSourcesToDownload(HashMap yaml) throws ParseException, ClassCastException{
        logger.info("parseSourcesToDownload(...yaml...)");
        
        HashMap<String, String> result = new HashMap<>();
        
        for(String key : (Set<String>) yaml.keySet() ){
            Map source = (Map) yaml.get(key); 
            if(key.equals("rom")){
                AppConstants.setEArchivePath((String) source.get("filePath"));
            }
            if(key.equals("twrp")){
                AppConstants.setTwrpImgPath((String) source.get("filePath"));
            }
            logger.debug("--> url: {}, filePath: {}", source.get("url"), source.get("filePath"));
            result.put((String) source.get("url"), (String) source.get("filePath"));
        }
        return result;
    }
}