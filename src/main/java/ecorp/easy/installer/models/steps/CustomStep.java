/*
 * Copyright 2021 - ECORP SAS 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models.steps;

import java.util.ArrayList;

/**
 * This class represent a step with some instructions
 * but WITHOUT script or command to run
 * @author vincent Bourgmayer
 */
public class CustomStep extends BasicStep implements ICustomStep{

    private final String titleKey;
    private final String titleIconName;
    private final ArrayList<String> textContentKeys; 
    
    public CustomStep(String type, String nextStepKey, int stepNumber, String titleKey, String titleIconName, ArrayList<String> contentKeys) {
        super(type, nextStepKey, stepNumber);
        this.titleKey = titleKey;
        this.titleIconName = titleIconName;
        this.textContentKeys = contentKeys;
    }

    @Override
    public String getTitleKey() {
        return this.titleKey;
    }

    @Override
    public String getTitleIconName() {
        return this.titleIconName;
    }

    @Override
    public ArrayList<String> getTextContentKeys() {
        return this.textContentKeys;
    }
}
