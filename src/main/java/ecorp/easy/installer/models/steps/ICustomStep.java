/*
 * Copyright 2021 - ECORP SAS 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models.steps;

import java.util.ArrayList;

/**
 * This interface describe a Step that could 
 * have UI content's define in config file:
 * Title, title's icon, instructions, pictures
 * 
 * This is basically a step where user have to
 * do something on the phone
 * @author vincent Bourgmayer
 */
public interface ICustomStep extends IStep{
    
    /**
     * Get the resources's key to get Title
     * @return String 
     */
    public String getTitleKey();
    
    /**
     * Get the name of an images to use as icon
     * associated with a title
     * @return String
     */
    public String getTitleIconName();
    
    /**
     * Get the keys of content (instructions and pictures)
     * to load from resources
     * @return ArrayList<String> le list of keys
     */
    public ArrayList<String> getTextContentKeys();

}
