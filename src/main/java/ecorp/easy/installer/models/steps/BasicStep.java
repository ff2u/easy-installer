/*
 * Copyright 2021 - ECORP SAS 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models.steps;

/**
 * This class encapsulate basic data about a step of a process
 * @idea: this class and subclass could define setters to allow to update ConfigParser
 * and make it to be a builder design pattern
 * @author Vincent Bourgmayer
 */
public class BasicStep implements IStep{
    private final String type;
    private final int stepNumber; //= step index
    protected String nextStepKey; 

    /**
     * Create an instance of Step
     * @param type the type of the step
     * @param nextStepKey
     * @param stepNumber the number of the step in the whole process
     */
    public BasicStep(String type, String nextStepKey, int stepNumber) {
        this.type = type;
        this.nextStepKey = nextStepKey;
        this.stepNumber = stepNumber;
    }
    
    /**
     * Get the type of the step (Custom, CustomExecutable, Load, etc.)
     * @return String the value of the type
     */
    @Override
    public String getType() {
        return type;
    }

    
    /**
     * Get the index of the step in a given process
     * @return Int index of the step
     */
    @Override
    public int getStepNumber() {
        return stepNumber;
    }

    /**
     * Give the key of the step to load when this one will be achieved
     * @return String the next step key
     */
    @Override
    public String getNextStepKey() {
        return nextStepKey;
    }
    
    /**
     * Define the key of the next step to call.
     * @param nextStepKey String if null, the value is then LAST_STEP_KEY's value.
     */
    public void setNextStepKey(String nextStepKey) {
        if( nextStepKey == null )
            this.nextStepKey = LAST_STEP_KEY;
        else
            this.nextStepKey = nextStepKey;
    }
}
