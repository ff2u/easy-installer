/*
 * Copyright 2019-2021 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.stepControllers;

import ecorp.easy.installer.controllers.LogController;
import static ecorp.easy.installer.controllers.stepControllers.StepController.parentController;
import ecorp.easy.installer.graphics.Instruction;
import ecorp.easy.installer.models.Command;
import ecorp.easy.installer.models.steps.CustomExecutableStep;
import ecorp.easy.installer.tasks.CommandRunnerService;
import ecorp.easy.installer.utils.UiUtils;
import ecorp.easy.installer.graphics.FlashGlobalProgressManager;
import ecorp.easy.installer.models.steps.CommandExecutionResult;
import ecorp.easy.installer.models.steps.IStep;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * This is clearly a mix of CustomStepController & ExecutableStepController
 * It means that it's code duplication. I don't like it but I didn't find a way to reuse
 * both class because multiple extension isn't possible and any design pattern fit
 * my need. But If you find a way please implement it!
 * @author vincent Bourgmayer
 */
public class CustomExecutableController extends StepController<CustomExecutableStep> implements Stoppable{
    
    protected final ResourceBundle IMG_BUNDLE = ResourceBundle.getBundle("instructions.imageName",
        new Locale.Builder()
       .setRegion("en")
       .setLanguage("EN")
       .setVariant(parentController.getPhone().getInternalCode())
       .build());
    
    private int currentEmphasizedInstruction = 0;
    private int instructionsCount;
    private CommandRunnerService backgroundService;

    @FXML private VBox instructionsContainer;
    @FXML private Label titleLbl;
    @FXML private ImageView imgView;
    @FXML private LogController logRootController;
    
    // progress bar's node of global Flash's process :
    @FXML HBox globalProgressIndicator;
    private FlashGlobalProgressManager globalProgressMgr;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb); 
        
        parentController.setNextButtonVisible(true);
        
        /** Initialization relatives to Custom **/
        titleLbl.setText(i18n.getString(step.getTitleKey() ));
        if(step.getTitleIconName() != null){
            titleLbl.setGraphic(new ImageView(UiUtils.loadImage(step.getTitleIconName())));
            titleLbl.setGraphicTextGap(18); //set spacing between icon and text
            titleLbl.setContentDisplay(ContentDisplay.LEFT); //set Icon to be displayed on left
        }
        
        final ArrayList<Instruction> instructions = loadInstructions();
        instructions.get(currentEmphasizedInstruction).emphasize();
        instructionsContainer.getChildren().addAll(instructions);
        displayInstructionImage(instructions.get(currentEmphasizedInstruction));
        instructionsCount = instructions.size();
        
        globalProgressMgr = new FlashGlobalProgressManager(parentController.getPhone().getFlashingProcess().getStepsCount());
        for(int i = 0;i < step.getStepNumber(); i++){
            globalProgressMgr.updateProgression();
        }
        globalProgressIndicator.getChildren().addAll(globalProgressMgr.getSegments());
        
        
        /** Initialization relatives to Executable **/
        final Command cmd = step.getCommand();
        backgroundService = new CommandRunnerService(cmd);
        

        
        backgroundService.setOnSucceeded(eh->{
            CommandExecutionResult result = backgroundService.getValue();
            
            if(result.isSuccess())
                onStepEnd();
            else{
                String errorMsgKey = cmd.getErrorMsg(result.getExitCode());
                if (errorMsgKey == null || errorMsgKey.isEmpty()){
                    errorMsgKey = "script_error_unknown";
                }
                displayError(errorMsgKey);
            }

        });
        backgroundService.start();

    }
    
    @Override
    protected void onContinueClicked(){
        logger.debug("onContinueClicked");

        final Instruction currentInstruction = (Instruction) instructionsContainer
                .getChildren().get(1+currentEmphasizedInstruction++);//the +1 is to ignore the title
        currentInstruction.trivialize();

        
        final Instruction nextInstruction = (Instruction) instructionsContainer
                .getChildren().get(1+currentEmphasizedInstruction); //the +1 is to ignore the title
        nextInstruction.emphasize();

        displayInstructionImage(nextInstruction);
            
        if(currentEmphasizedInstruction+1 == instructionsCount){
            logger.debug("last instruction reached instruction");
            parentController.setNextButtonVisible(false);
        }
    }
    
    
    /**
     * Create Instruction nodes with text key from step
     */
    private ArrayList<Instruction> loadInstructions(){
        final ArrayList<Instruction> result = new ArrayList<>();
        
        for(String key : step.getTextContentKeys()){
            String contentKey = "all_lbl_notImplemented"; //set default key
            System.out.println("translation key: "+key);
            if(i18n.containsKey(key)){
                contentKey = key;
            }
            result.add(new Instruction(key, i18n.getString(contentKey)));
        }
        return result;
    }
    
    
    /**
     * Display Image corresponding to the current Instruction
     * if there is one. In other way, it remove current image
     * @param instruction 
     */
    private void displayInstructionImage(Instruction instruction){
        if(IMG_BUNDLE.containsKey(instruction.getKey())){
            imgView.setImage( 
                UiUtils.loadImage( IMG_BUNDLE.getString( instruction.getKey() ) ) );
        }else{
            imgView.setImage(null);
        }
    }
    
    /**
     * Display an error due to background task 
     * @param errorMsgKey 
     */
    protected void displayError(String errorMsgKey){
        
        parentController.resetNextButtonEventHandler(); 
        //parentController.setNextButtonVisible(false);
        //parentController.setIsFlashed(false);
        parentController.setCurrentStepKey(IStep.LAST_STEP_KEY);
        
        if(i18n.containsKey(errorMsgKey)){
            final Label errorLbl = new Label(i18n.getString(errorMsgKey));
            ((VBox) uiRoot).getChildren().add(2, errorLbl);
        }else{
            logger.error("Missing translation for error key: {}", errorMsgKey);
        }
        UiUtils.hideNode(instructionsContainer.getParent());
        
        logRootController.showSendLogBtn();
                
        Button tryAgainBtn = new Button(i18n.getString("all_lbl_tryAgain"));
        tryAgainBtn.setOnMouseClicked(( MouseEvent event) -> {
            parentController.retryToFlash();
        });
        ((VBox)uiRoot).setAlignment(Pos.TOP_CENTER);
        ((VBox)uiRoot).getChildren().add(3, tryAgainBtn);
    }

    @Override
    public void stop() {
        logger.debug("CustomExecutableController.stop()");
        backgroundService.cancel();
    }
}