/*
 * Copyright 2021 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.tasks;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.models.Command;
import ecorp.easy.installer.models.steps.CommandExecutionResult;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.concurrent.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class encapsulate the task to execute a command in a process
 * @author vincent Bourgmayer
 */
public class CommandExecutionTask extends Task<CommandExecutionResult> {
    protected final static Logger logger = LoggerFactory.getLogger(CommandExecutionTask.class);
    final protected static char COMMON_PARAM_IDENTIFIER = '$';//allow to identify when an param of a step is to load from common parameter
    final protected static Pattern REGEX_FIND_PARAM = Pattern.compile("\\$\\{(.*?)\\}");
    final protected Command command; //The command to run
    protected String shellOutput; //The stdout of the process
    protected int exitCode; //The result code of the command
    Process pc;

    
    private final static HashMap<String, String> COMMON_PARAMS = new HashMap<>();
    
    static {
        final String sourcePath = AppConstants.getSourcesFolderPath();
        
        COMMON_PARAMS.put("SOURCES_PATH", sourcePath);
        COMMON_PARAMS.put("ADB_FOLDER_PATH", AppConstants.getADBFolderPath());
        COMMON_PARAMS.put("HEIMDALL_FOLDER_PATH", AppConstants.getHeimdallFolderPath());
        COMMON_PARAMS.put("JAVA_FOLDER_PATH", AppConstants.JavaHome);
    }
    
    /**
     * Instanciate a CommandExecutionTask
     * @param command the Command to execute
     */
    public CommandExecutionTask(Command command){
        this.command = command;
    }

     /**
     * run the command
     * @return always null because "P" is for subclass
     * @throws Exception 
     */
    @Override
    protected CommandExecutionResult call() throws Exception {
        CommandExecutionResult result;
        
        try{
            executeCommand( getProcessBuilder() );
        }catch(IOException | InterruptedException e){
            cancel();
            logger.error("cmd.execAndReadOutput(), error = {}", e.toString() );
            return new CommandExecutionResult(false);
        }
        
        logger.debug("Exit value = {}\n", exitCode);
        
        boolean success;
        //Handle success
        if( (command.getOkCodes() == null && exitCode == 0 )
                || command.getOkCodes().containsKey(exitCode) ){
            String outputKey = command.getOutputKey();
            //If an output is expected from succeed script

            if( outputKey != null && outputKey.charAt(0) == COMMON_PARAM_IDENTIFIER){
                final int lastIndex = shellOutput.lastIndexOf("\n");
                final int length = shellOutput.length();
                final String shellOutputCleaned = shellOutput.substring(lastIndex+1, length);
                logger.debug("shell output cleaned : "+shellOutputCleaned);
                COMMON_PARAMS.put(outputKey.replace("${","").replace("}", ""), shellOutputCleaned);
            }
            success= true;
        }else{
            success= false;
        }
        
        result = new CommandExecutionResult(success);
        result.setExitCode(exitCode);
        result.setShellOutput(shellOutput);
        return result;
    }

    /**
     * Execute the command
     * @param pb
     * @throws IOException 
     * @throws java.lang.InterruptedException 
     */
    protected void executeCommand(ProcessBuilder pb) throws IOException, InterruptedException{
        pc= pb.start();
        
        /* Collect stdout for further usage */
        final StringBuilder sb = new StringBuilder();
        
        try( InputStream stdout = pc.getInputStream();
        InputStreamReader isr = new InputStreamReader (stdout);
        BufferedReader br = new BufferedReader(isr); ){
            String line;
            
            while(pc.isAlive() && !this.isCancelled()){
                line = br.readLine();
                
                //I don't remember the reason of the second part of the below test...
                if( line != null && !line.equals("null") ){
                    sb.append("\n\n").append(line);
                    logger.debug("\n  (debug)"+line);
                }
            }
            
            this.exitCode = pc.exitValue();
        }catch(IOException e){
            logger.error("execAndReadOutput(), error = {}", e.toString() );
            this.exitCode = -1;
        }

        this.shellOutput = sb.toString();
        
        if(pc.isAlive())       
            pc.destroy();
    }

    /**
     * Build the ProcessBuilder to execute Command
     * @return ProcessBuilder instance containing the full command to run
     */
    protected ProcessBuilder getProcessBuilder(){
        final ProcessBuilder pb = new ProcessBuilder(getFullCmd().split(" "));
        pb.redirectErrorStream(true);
        return pb;
    }
    
    /**
     * Concatenate command base with parameters to obtain
     * the full command to be run
     * @return String the full command
     */
    protected String getFullCmd(){
        final StringBuilder sb = new StringBuilder();
        
        // Prepare base of the command
        String cmdBase = command.getCommandBase();
        if(AppConstants.isWindowsOs()){
            cmdBase = "cmd.exe /c \"\""+cmdBase+"\"";
        }
        sb.append(cmdBase);
        
        updateParameters();
        
        //Add the parameters
        if(command.getParameters() != null && !command.getParameters().isEmpty()){
            command.getParameters().values().forEach((param) -> {
                if(AppConstants.isWindowsOs()){
                    param = "\""+param+"\"";
                }
                sb.append(" ").append(param);
            });
        }
        // Close the full command
        if(AppConstants.isWindowsOs()){
            sb.append("\"");
        }

        logger.debug("getFullCmd(), full command =  {}", sb.toString());
        return sb.toString();
    }
    
    
        
    
    /**
     * Update parameters of the current command
     * It is called before to execute the command
     */
    protected void updateParameters(){
        if(command.getParameters() != null){ //@TODO: remove functionnal and rewrite it as it was before with simple loop.
            command.getParameters().entrySet().stream().filter((param) -> (param.getValue().contains("$"))).forEachOrdered((param) -> {
                Matcher matcher = REGEX_FIND_PARAM.matcher(param.getValue());
                while(matcher.find()){
                    command.addParameter(param.getKey(), param.getValue().replace(matcher.group(0), COMMON_PARAMS.get(matcher.group(1))));
                }
            });

            logger.debug("updateParameters(), Parameters = "+command.getParameters().toString());
        }     
    }
    
    
    
    /**
     * Write a new value for the associated key.
     * If the value is already present, the precedent value will be overwritten
     * @param key the key to identify a param
     * @param value  the value of the param
     */
    public static void updateCommonParam(String key, String value){
            COMMON_PARAMS.put(key, value);
    }
    
    @Override protected void cancelled() {
        super.cancelled();
        logger.info("cancelled()");
        if(pc != null && pc.isAlive())       
            pc.destroy();
    }
    
    
    /* Getter */
    
    public Command getCommand() {
        return command;
    }

    public String getShellOutput() {
        return shellOutput;
    }

    public int getExitCode() {
        return exitCode;
    }

}
