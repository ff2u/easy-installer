/*
 * Copyright 2019-2021 - ECORP SAS 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.tasks;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.exceptions.TooManyDevicesException;
import ecorp.easy.installer.models.Command;
import ecorp.easy.installer.models.Phone;
import ecorp.easy.installer.models.steps.CommandExecutionResult;
import java.util.regex.Pattern;
import javafx.concurrent.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provide task which run ADB command in background to detect that a device is reachable
 * 
 * First design made this class to extend CommandExecutionTask<P> but since i defined
 * CommandExecutionTask<CommandExecutionResult> this isn't possible anymore. Instead,
 * this task will call the CommandExecutionTask in the same thread as a blocking job. Thanks to a future.
 * 
 * @author vincent Bourgmayer
 * @author Ingo
 */
public class DeviceDetectionTask extends Task<Phone>{
    private final static Logger logger = LoggerFactory.getLogger(DeviceDetectionTask.class);
    private final static Pattern LINEBREAK_PATTERN = Pattern.compile("\\R");
    private final static Pattern SPACE_PATTERN = Pattern.compile("\\s+");
    private final Command command;

    
    public DeviceDetectionTask(){
        command = new Command(AppConstants.getADBFolderPath()+"adb");
        command.addParameter("1", "devices");
        command.addParameter("2", "-l");
    }
    
    /**
     * @return 
     * @throws java.lang.Exception
     * @inheritDoc 
     */
    @Override
    protected Phone call() throws Exception{
        Phone detectedDevice = null;

        detectedDevice = runAdbDevicesCmd();

        if(detectedDevice != null){
            logger.debug("call(), result: Device found");
        }
        return detectedDevice;
    }
    
    /**
     * Run "adb devices -l" command
     * @param baseCmdString
     * @return
     * @throws Exception 
     */
    private Phone runAdbDevicesCmd() throws Exception{
        logger.info("runADBDevicesCmd(): ", command.getCommandBase());

        
        //@TODO use CommandExecutionTask instead of Command
        CommandExecutionTask task = new CommandExecutionTask(command);

        task.run(); //I feel like it lacks something... but it work in test
        
        CommandExecutionResult result = task.get();
        
        Phone detectedDevice = null;
        String[] outputs = LINEBREAK_PATTERN.split(result.getShellOutput());
        logger.debug(" raw shell outputs = {} ", result.getShellOutput());
        if(outputs.length <=1){
            logger.info("  Waiting");
            Thread.sleep(2250);
            return null;
        }
        int counter =0;
        for(String s : outputs){
            if(s.isEmpty() || "List of devices attached".equals(s)) continue;
            Phone deviceFound = checkAdbDevicesResult(s);
            if(deviceFound != null){
                detectedDevice = deviceFound;
                ++counter;
            }
        }
        if(counter > 1){
            logger.info("  Too many devices detected");
            throw new TooManyDevicesException(counter);
        }
        if(detectedDevice==null) {
            logger.info("  waiting");
            Thread.sleep(2250);
        }
        return detectedDevice;
    }
    
    
    /**
     * Analyse one line from result of "ADB devices -l" command
     * @param resultLine
     * @return the adb's device's id if found else it return empty string
     */
    private Phone checkAdbDevicesResult(String resultLine){
        logger.debug("checkAdbDevicesResult({})", resultLine);
        Phone result = new Phone();
        if(resultLine.contains("unauthorized")){
            logger.info("   Unauthorized device found");
            result.setAdbAuthorized(false);
        }
        else if(resultLine.contains("device") || resultLine.contains("recovery")){
            logger.info("  Device has been found");
           
            //Split string on each space
            String[] datas = SPACE_PATTERN.split(resultLine);
            result.setSerialNo(datas[0]);
            result.setAdbAuthorized(true); 
            
            //loop over each subString
            for(String stringPart : datas){
                logger.debug("  Current subString : "+stringPart);
                if(stringPart.contains("product:")){
                     logger.debug("  \"product\" keyword has been found");
                     result.setAdbProduct(stringPart.substring("product:".length() ));
                }else if(stringPart.contains("model:")){
                    logger.debug("  \"model\" keyword has been found");
                    String model = stringPart.substring("model:".length() );
                    //if(model.equals("Volla_Phone")) model = "GS290";
                    result.setAdbModel(model);
                }else if(stringPart.contains("device:")){
                    logger.debug("  \"device\" keyword has been found");
                    String device = stringPart.substring("device:".length() );
                    if(device.equals("2e")) device ="Teracube_2e"; 
                    //if(device.equals("k63v2_64_bsp")) device="GS290"; //bad idea. Device not really compatible.
                    result.setAdbDevice(device);
                }
            }
        }else{
            return null;
        }
        return result;
    }
}