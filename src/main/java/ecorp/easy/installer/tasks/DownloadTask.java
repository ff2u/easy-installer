/*
 * Copyright 2019-2020 - ECORP SAS 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.tasks;

import ecorp.easy.installer.AppConstants;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.concurrent.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * this class verify the checksum of a file and download it
 * @author vincent Bourgmayer
 * @author Ingo
 */
public class DownloadTask extends Task<Boolean>{
    private final static String checkSumExtension = ".sha256sum";
    private final static Logger logger = LoggerFactory.getLogger(DownloadTask.class);
    /**
     * Constant size
     */
    private static final long[] CST_SIZE = {1024, 1024*1024, 1024*1024*1024, 1024*1024*1024*1024};
    /**
     * Constants units
     */
    private static final String[] CST_UNITS = {"KB", "MB", "GB", "TB"};
    
    final private ResourceBundle i18n;
    final private String targetUrl;
    final private String fileName;
    
    /**
     * COnstruction of the download task
     * @param targetUrl the web path to the resource
     * @param fileName name of the file
     * @param resources used to send already translated message
     */
    public DownloadTask(String targetUrl, String fileName, ResourceBundle resources){
        this.targetUrl = targetUrl;
        this.fileName = fileName;
        this.i18n = resources;
    }
    /**
     * @inheritDoc 
     * @return Boolean object
     * @throws Exception 
     */
    @Override
    protected Boolean call() throws Exception {

        //build local filePath
        final String localFilePath = AppConstants.getSourcesFolderPath()+fileName;
        
        //Check if already exist and integrity
        final String checksumFilePath = localFilePath+checkSumExtension;
        
        if(isCancelled()) return false;
        
        this.updateTitle("Downloading "+fileName+checkSumExtension);
        
        
        File checksumLmdFile = new File(AppConstants.getSourcesFolderPath()+"lmd."+fileName+checkSumExtension);
        checksumLmdFile.createNewFile();
        
        // Download checksum. If file not downloaded return false and stop downloadin because integrity isn't guaranteed
        if( !downloadFile(targetUrl+checkSumExtension, checksumFilePath,checksumLmdFile) ){
            updateMessage(i18n.getString("download_lbl_cantcheckIntegrity"));
            return false;
        }

        //If checksum valid it means that file is already there and up to date
        updateMessage(i18n.getString("download_lbl_checkingIntegrity")); 
        if ( validChecksum(checksumFilePath) ){
            updateMessage(i18n.getString("download_lbl_fileAlreadyUptoDate"));
            return true;
        }

        if(isCancelled()) return false;

        this.updateTitle("Downloading "+fileName);

        final String tmpFilePath = AppConstants.getSourcesFolderPath()+"tmp."+fileName;
        
        File lmdFile = new File(AppConstants.getSourcesFolderPath()+"lmd."+fileName); //used to Store last modified Date of remote content
        lmdFile.createNewFile();
        
        if ( downloadFile(targetUrl, tmpFilePath, lmdFile) )//Download file
        {
            logger.debug("Downloaded succeed. Rename temp file to right fileName");
            File tmpFile = new File(tmpFilePath);
            tmpFile.renameTo(new File(localFilePath));
            return validChecksum(checksumFilePath);

        }else{
            updateMessage(i18n.getString("download_lbl_downloadError"));
            return false;
        }
    }
    
    // method link to file downloading
    
    /**
     * Read lastmodified date of the remote file at previous download of the file
     * Only uncompletly downloaded files are concerned
     * @param lmdFile
     * @return
     * @throws FileNotFoundException
     * @throws IOException 
     */
    private String readLastModifiedFrom(File lmdFile) throws FileNotFoundException, IOException{
        String line;
        try (BufferedReader reader = new BufferedReader(new FileReader(lmdFile))) {
            line = reader.readLine();
        }
        return line;
    }
    
    /**
     * Write the last modified of the remote file for future use
     * @param lmdFile
     * @param timestamp
     * @throws IOException 
     */
    private void writeLastModified(File lmdFile, long timestamp) throws IOException{
        try (FileWriter fileWriter = new FileWriter(lmdFile,false)) {
            
            String lmd = DateTimeFormatter.RFC_1123_DATE_TIME
                    .withZone(ZoneId.of("GMT"))
                    .withLocale(Locale.ENGLISH)
                    .format(Instant.ofEpochMilli(timestamp));
            fileWriter.write(lmd);
        }
    }
    
    /**
     * Perform the downloading of the specified file
     * If a part is already downloaded, then the download resume from previous state
     * @return boolean true if file has been successfully downloaded, false either
     */
    private boolean downloadFile(final String fileURL, final String localFilePath, File lmdFile) throws  MalformedURLException, IOException, InterruptedException{
        logger.debug("downloadFile({}, {})",fileURL,localFilePath  );
        
        long previouslyDownloadedAmount = 0;
        
        //Build the query
        HttpURLConnection connect = (HttpURLConnection) new URL(fileURL).openConnection();
        connect.setReadTimeout(30000);
        connect.setConnectTimeout(30000);
        
        File localFile = new File(localFilePath);
        if(localFile.exists()){
            previouslyDownloadedAmount = localFile.length();
            logger.debug("local file exist, size is {}", localFile.length());
            
            String lmd = readLastModifiedFrom(lmdFile);
            String lastModifiedDate = (lmd != null) ? lmd : new Date(localFile.lastModified() ).toString();
            logger.debug("last modified date = {}", lastModifiedDate);
            connect.setRequestProperty("If-Range", lastModifiedDate );
            connect.setRequestProperty("Range", "bytes=" + previouslyDownloadedAmount + "-");
        }
        
        //Perform the query and analyze result
        final int responseCode = connect.getResponseCode();
        final boolean canAppendBytes = (responseCode == HttpURLConnection.HTTP_PARTIAL);
        logger.debug("response code: {}, {}", connect.getResponseCode(), connect.getResponseMessage());
        final long lastModified = connect.getLastModified();
        
        if( !canAppendBytes ){
            if( responseCode == HttpURLConnection.HTTP_OK )  { //return false it resources is unreachable
                writeLastModified(lmdFile, lastModified);
            }else{
                return false;
            }
            previouslyDownloadedAmount = 0; //set it back to 0 in case it contains size of old content
        }

        //Get remote file Size
        final double fileSize = connect.getContentLengthLong();
        logger.debug("remote fileSize = {}", fileSize);
        
        final double fullFileSize = fileSize+previouslyDownloadedAmount;
        logger.debug("full file size = {}", fullFileSize);
        
        //Update UI
        final String formattedFileSize = formatFileSize(fullFileSize); //used for UI
        updateProgress(-1, fullFileSize);
        updateMessage(formatFileSize(previouslyDownloadedAmount)+" / "+formattedFileSize );
        
        boolean downloaded = false;
        
        try(FileOutputStream fos = new FileOutputStream(localFilePath,canAppendBytes);
            InputStream is = connect.getInputStream();
            ReadableByteChannel rbc = Channels.newChannel(connect.getInputStream()); 
        ){
            //Start the timeOutThread which will stop a blocked download
            TimeOutRunnable timeoutRunnable = new TimeOutRunnable();
            Thread timeoutThread = new Thread(timeoutRunnable);
            timeoutThread.setDaemon(true);
            timeoutThread.start();
            
            long downloadAmount =  previouslyDownloadedAmount;
            while ( rbc.isOpen() && !isCancelled() && ! downloaded ){

                final long precedentAmount = downloadAmount;
                downloadAmount += fos.getChannel().transferFrom(rbc,downloadAmount,1 << 18);
               
                if(precedentAmount == downloadAmount){ //it means nothing had been downloaded
                    logger.warn("precedent amount = downloaded amount");
                    downloaded = false;
                    rbc.close();
                    connect.disconnect();
                }else{
                    timeoutRunnable.amountIncreased(); //delay the timeout
                    
                    updateProgress(downloadAmount, fullFileSize);
                    updateMessage( formatFileSize(downloadAmount)+" / "+formattedFileSize);

                    fos.flush();
                    downloaded = (downloadAmount == fullFileSize);
                }
            }
            //end of download, stop the timeout thread
            timeoutRunnable.stop();
        }

        if(downloaded) lmdFile.delete(); //Download complete, so we could remove this file
        return (!isCancelled() && downloaded);
    }
    
    /**
     * Format file size to use correct size name (mb, gb, ...)
     * @todo definitively should be in the UI
     * @param value the file size formatted witht the good size category
     * @return 
     */
    public String formatFileSize(final double value){
        double size;
        for (int i = 0; i < 3; i++) {
            size=value/CST_SIZE[i];
            if (size <= 1024) {
                return new DecimalFormat("#,##0.#").format(size) + " " + CST_UNITS[i] ;
            }
        }
        return null;
    }
    
    
    //Method about file checking   
    /**
     * read the content of the checksum file
     * @param fileChecksum file containing checksum
     * @return null if no content. else a line in following format: checksum relativefilePath
     */
    private String readChecksumFile(String fileChecksum) throws IOException{
        Scanner sc = new Scanner(new FileReader(fileChecksum)); 
        if(sc.hasNextLine()){
          return sc.nextLine();
        }
        return null;
    }

    /**
     * Verify the integrity of the downloaded file
     * source: http://www.sha1-online.com/sha256-java/
     * @param checksumFilePath path of the checksum file
     * @return true if integrity has been validated
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    private boolean validChecksum( String checksumFilePath) throws NoSuchAlgorithmException, IOException{
        logger.debug("validChecksum("+checksumFilePath+")");
        //get file containing checksum
        File checksumFile = new File(checksumFilePath);
        if( !checksumFile.exists()){
            logger.debug("checksum file doesn't exist");
            return false;        //If checksum file doesn't exist we can't validate checksum
        }
        
        //read content of file containing checksum to extract hash and filename
        String checksumLine = readChecksumFile(checksumFilePath);
        if(checksumLine == null) return false;
        logger.debug("  ChecksumLine = "+checksumLine);
        String[] splittedLine = checksumLine.split("\\s+"); //@todo use pattern & matcher

        //check local file exist
        File file = new File(AppConstants.getSourcesFolderPath()+splittedLine[1]);
        if(!file.exists()){         //if file concerned by checksum doesn't exist we can't validate
            updateMessage(i18n.getString("download_lbl_localFileNotFound")); //@todo not sure it is required...
            logger.debug("  "+splittedLine[1]+" do not exists");
            return false;
        }
        
        updateProgress(-1,1); //@todo should be call elsewhere. probably in Controller
        String computedChecksum = createFileChecksum(file);
        
        logger.debug("compare checksum: "+computedChecksum+" vs "+splittedLine[0]);
        return computedChecksum.equals(splittedLine[0]);
    }
    
    /**
     * Compute checksum of the given file
     * @param file File for which we want the checksum
     * @return the checksum of the file
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    private String createFileChecksum(File file) throws NoSuchAlgorithmException, IOException{
        logger.debug("createFileChecksum()");
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        FileInputStream fis = new FileInputStream(file);
  
        byte[] data = new byte[1024];
        int read = 0;
        
        while ((read = fis.read(data)) != -1) { sha256.update(data, 0, read); }
        byte[] hashBytes = sha256.digest();
  
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hashBytes.length; i++) {
          sb.append(Integer.toString((hashBytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
    
    
    /**
     * Private inner class used to set a timeout on File's download
     */
    private class TimeOutRunnable implements Runnable{

        final long timeout = 10000; //10secondes
        long currentTime;
        boolean stop = false;

        synchronized void stop(){
            this.stop = true;
        }

        @Override
        public void run() {
            
            currentTime = System.currentTimeMillis();
            long previousTime;
            while(!stop){
                previousTime = currentTime;
                //isCancelled() is a method of the containing DownloadTask.java
                if(Thread.interrupted() || isCancelled() ) stop = true;
                try{
                    Thread.sleep(timeout); 
                    if(!stop && currentTime == previousTime){
                        logger.info("No updates");
                        //updateProgress & updateMessage are methos of DownloadTask.java
                        updateProgress(-1, 1);
                        updateMessage(i18n.getString("download_lbl_connectionLost"));
                    }
                }catch(Exception e){
                    stop = true;
                    logger.error("TimeoutThread crashed: "+e.toString());
                }
            }
            logger.debug("timeoutThread is over!");
        }
        //Signal that an amount was increased
        synchronized private void amountIncreased(){
            currentTime = System.currentTimeMillis();
        }
    };
}