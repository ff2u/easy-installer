#Include Modern UI and others
#--------------------------------
!include LogicLib.nsh
!include "WordFunc.nsh"
  !insertmacro VersionCompare
!include "MUI2.nsh"

# General settings
#--------------------------------
RequestExecutionLevel admin #if 'user' then it can't install in C:\Program files
!define MUI_ICON "buildSrc/windows/easy-installer.ico"
!define appVersion "v0.13.4-beta"
Name "Easy-installer ${appVersion}"
# define installation directory
InstallDir "$PROGRAMFILES64\easy-installer"
InstallDirRegKey HKCU "Software\ecorp\easy-installer" "installDir"
# name the installer
OutFile "Easy-installer-setup.exe"

Icon "buildSrc/windows/easy-installer.ico"
# Pages
#--------------------------------
!define MUI_WELCOMEFINISHPAGE_BITMAP "buildSrc/windows/welcomePage.bmp"
!define MUI_WELCOMEPAGE_TITLE "Welcome to the easy-installer setup wizard"
!define MUI_WELCOMEPAGE_TEXT  "The easy-installer will allow to flash /e/ OS on compatible device"
!insertmacro MUI_PAGE_WELCOME

!define MUI_PAGE_HEADER_TEXT "Licence agreement"
!define MUI_PAGE_HEADER_SUBTEXT "Please review the licence terms before installing easy-installer"
!define MUI_LICENSEPAGE_TEXT_TOP "Scroll down to read the rest of the agreement"
!define MUI_LICENSEPAGE_TEXT_BOTTOM "If you accept the terms of the agreement, click 'I agree' to continue. You must accept the agreement to setup easy-installer"
!insertmacro MUI_PAGE_LICENSE "LICENSE"

!define MUI_PAGE_HEADER_TEXT "Choose easy-installer location"
!insertmacro MUI_PAGE_DIRECTORY
#!insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
!insertmacro MUI_PAGE_INSTFILES
!define MUI_FINISHPAGE_TITLE "Set up completed"
!define MUI_FINISHPAGE_TEXT "You can click on the 'close' button to exit"
!define MUI_FINISHPAGE_BITMAP "buildSrc/windows/welcomePage.bmp"
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

# Behaviour
#--------------------------------
Function .onInit
ReadRegStr $0 HKCU "Software\ecorp\easy-installer\uninstallTest" "uninstaller_loc"
${If} $0 != ""
	ReadRegStr $1 HKCU "Software\ecorp\easy-installer" "version"
	${VersionCompare} $1 ${appVersion} $2
	${If} $2 == 2
		${If} ${Cmd} `MessageBox MB_YESNO|MB_ICONQUESTION "An older version is installed. Do you want to override it ?" /SD IDYES IDNO`
			Quit
		${EndIf}
	${Else}
		MessageBox MB_OK|MB_ICONEXCLAMATION "You already have the latest version"
		Quit
	${EndIf}
${EndIf}
FunctionEnd


# Section
#--------------------------------

# Default section start; every NSIS script has at least one section
Section
	SetOutPath "$INSTDIR"
	# Adds file(s) to be extracted to the current output path ($OUTDIR).
	File /r "build/image/easy-installer-windows-x64/*"
	File "buildSrc/windows/easy-installer.ico"
	# create the uninstaller
    WriteUninstaller "$INSTDIR\uninstall.exe"
    # create a shortcut named "easy-installer" in the start menu programs directory
    # point the new shortcut at the program uninstaller
    CreateShortcut "$SMPROGRAMS\easy-installer.lnk" "$INSTDIR\bin\javaw.exe" "-m ecorp.easy.installer/ecorp.easy.installer.EasyInstaller" "$INSTDIR\easy-installer.ico" 0
    # create a shortcut named "easy-installer-uninstall" in the start menu programs directory
    # point the new shortcut at the program uninstaller
    CreateShortcut "$SMPROGRAMS\easy-installer-uninstall.lnk" "$INSTDIR\uninstall.exe"
	WriteRegStr HKCU "Software\ecorp\easy-installer" "installDir" $INSTDIR
	WriteRegStr HKCU "Software\ecorp\easy-installer" "version" ${appVersion}
	WriteRegStr HKCU "Software\ecorp\easy-installer\uninstallTest" "uninstaller_loc" "$INSTDIR\uninstall.exe"
	WriteRegStr HKCU "Software\ecorp\easy-installer\uninstallTest" "quietUninstaller_loc" '"$INSTDIR\uninstall.exe" /S'
    # Add uninstaller in "add/remove" control panel
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\easy-installer" "DisplayName" "Easy-installer"
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\easy-installer" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\easy-installer" "DisplayIcon" "$\"$INSTDIR\easy-installer.ico$\""
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\easy-installer" "InstallLocation" "$\"$INSTDIR$\""
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\easy-installer" "Publisher" "ECORP SAS"
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\easy-installer" "DisplayVersion" "${appVersion}"
SectionEnd
# Uninstaller
Section "uninstall"
	 # first, delete the uninstaller
    Delete "$INSTDIR\uninstall.exe"
    # second, remove the link from the start menu
    Delete "$SMPROGRAMS\easy-installer-uninstall.lnk"
    Delete "$SMPROGRAMS\easy-installer.lnk"
	#remove the installation directory
    RMDir /r "$INSTDIR"
    RMDir /r "$LOCALAPPDATA\easy-installer"
    #clean the registry
    DeleteRegKey  HKCU "Software\ecorp\easy-installer\uninstallTest"
    DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\easy-installer"
    DeleteRegKey  HKCU "Software\ecorp\easy-installer"
SectionEnd
