#!/bin/bash

JAVA_VERSION=$(cat $JAVA_VERSION_FILE | grep "appTitle" | sed -E 's/.*"(v.*)".*/\1/')
echo "Java version:      $JAVA_VERSION ($JAVA_VERSION_FILE)"

SNAPCRAFT_VERSION=$(cat $SNAPCRAFT_VERSION_FILE | grep "^version" | sed -E "s/^version: '(v.*)'.*/\1/")
echo "Snapcraft version: $SNAPCRAFT_VERSION ($SNAPCRAFT_VERSION_FILE)"

WINDOWS_VERSION=$(cat $WINDOWS_VERSION_FILE | grep "define appVersion" | sed -E 's/!define appVersion "(v.*)"/\1/')
echo "Windows version:  $WINDOWS_VERSION ($WINDOWS_VERSION_FILE)"

echo "Tag version:       $CI_COMMIT_TAG"

if [ $JAVA_VERSION != $CI_COMMIT_TAG ] || [ $SNAPCRAFT_VERSION != $CI_COMMIT_TAG ] || [ $WINDOWS_VERSION != $CI_COMMIT_TAG ]
then
  exit 1
fi
