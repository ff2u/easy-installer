# Easy Installer
**Current Version:** v0.13.4-beta
**Current released Version:** v0.13.4-beta

Members:
- Gaël
- Romain
- Alexis
- Vincent
- Arnau
- Manoj


Community:
- Ingo
- steadfasterX
- André Lam
- Omer Akram
- Alandour
- Mex Salem
- F. Wildermuth
- Anonymous
- Nikolay Sinyov
- avtkal
- Paolo Pantaleo
- Tim de Boer
- Edoardo Regni
- Porrumentzio
- Piero
- GunChleoc
<<<<<<< HEAD
- Volker
=======
- ff2u
>>>>>>> c52c1e5db7926d546c6eda664bde8d4557b6e6f3

Reviewer
- Alexandre
- Sooraj
- Romain
- Kumar

developer: 
- [Vincent Bourgmayer](vincent.bourgmayer@e.email)
- Israel Yago pereira

## Changelogs
### v0.13.4-beta (current - unreleased)
- Fix Ubuntu build's docker image - by Israel & Omer Akram & Nicolas
- Refactor classes related to script execution
- Update Russian translations - by Nikolay Sinyov
- Update German translations - by F. Wildermuth
- Update Gaelic translations - by GunChleoc
- Teracube 2e support disabled (temporarily)
- Update Readme
- update version number to v0.13.4-beta

### v0.13.3-beta (latest release)
- Refactor user interface related to flashing device
- Refactor Configuration file
- Handle "Too many devices detected"
- Update version number to v0.13.3-beta
- Fixed e-mail registration API Url - by Israel Yago pereira
- Update Russian translations - by Nikolay Sinyov & Israel Yago pereira
- Update German translations - by F. Wildermuth
- Update Dutch translations - by Edoardo Regni
- Update config file for arch-linux build - by steadfasterX

### v0.13.2-beta
- Small refactoring of FlashSceneController.java & FlashScene.fxml
- Revert support of Volla phone
- Update translations
- Update dutch translations - by Edoardo Regni
- Update german translations - by F. Wildermuth
- Update russian translations - by Nikolay Sinyov
- Update Italian translations - by Paolo Pantaleo

## Documentation
To run directly from source:
`./gradlew run -Dorg.gradle.java.home=buildSrc/linux/jdk-11.0.2/`
from root of the project.

- [How to build from source with Gradle](../../wikis/Build-with-Gradle)
- [How to support new Device](../../wikis/Support-new-device)
- [How translation works](../../wikis/update-translation)

- [Weblate](https://i18n.e.foundation/projects/e/easy-installer/)



## Where to get sources (files flashed)
- windows: `C:\Users\<your username>\AppData\Local\easy-installer\sources\`
- linux: `~/snap/easy-installer/common/sources/`


## Where to get log at runtime
- Windows: `C:\Users\<your username>\AppData\Local\easy-installer`
- linux : `~/snap/easy-installer/common/`

## Techno dependancy
- Java
- JavaFX
- FXML
- YAML
- CSS
- Snapcraft
- NSIS

## Dependancy:
- Java 11+
- JavaFX 13+
- Flash-lib
- Gradle 4.10+
    - org.openjfx.javafxplugin
    - https://badass-jlink-plugin.beryx.org/
- SnakeYaml 1.24+ 
- Git
- Gitlab
- logback
- slf4j
